# CoKoala Portfolio
## Personal Portfolio 

Project to contain the following segments:

- Games
- Web Dev
- Data structures and Algo's
- Backend
- Databases
- App Dev
- WebGL implementation

### This Iteration of the Project

This is the third revision of my portfolio. Although I do prefer working on backend stuff - this has been an illuminating experience as I've never realised how many things can go wrong on the front end. I used VueJS, Nuxt, Vuetify to develop this portfolio and I've got the grounds setup for a database in the backend. It's a little overkill, but it's good for recruiters or those who are looking to interview me to see. 

                            Ali El Ali
